# Awesome List

-   [中文资源大全](https://github.com/jobbole)
-   [经典编程书籍大全](https://github.com/jobbole/awesome-programming-books)
-   [免费的编程中文书籍索引](https://github.com/justjavac/free-programming-books-zh_CN)
-   [awesome-awesomeness-zh_CN](https://github.com/justjavac/awesome-awesomeness-zh_CN)
-   [https://github.com/jnv/lists](https://github.com/jnv/lists)
-   [awesome-awesome-awesome](https://github.com/t3chnoboy/awesome-awesome-awesome)
-   [awesome](https://github.com/sindresorhus/awesome)
-   [Awesome Awesomeness](https://github.com/bayandin/awesome-awesomeness)
-   [bayandin/awesome-awesomeness](https://github.com/bayandin/awesome-awesomeness)
-   [awesome-all](https://github.com/bradoyler/awesome-all)
-   [awesome-awesome](https://github.com/emijrp/awesome-awesome)
-   [erichs/awesome-awesome](https://github.com/erichs/awesome-awesome)
-   [oyvindrobertsen/awesome-awesome](https://github.com/oyvindrobertsen/awesome-awesome)
-   [fleveque/awesome-awesomes](https://github.com/fleveque/awesome-awesomes)
-   [coopermaa/awesome-awesome](https://github.com/coopermaa/awesome-awesome)

# Awesome-\*

### Git

-   [GitHub 秘籍](https://github.com/tiimgreen/github-cheat-sheet/blob/master/README.zh-cn.md)
-   [awesome-git](https://github.com/dictcp/awesome-git)

### Linux

-   [awesome-linux-system](https://github.com/aleksandar-todorovic/awesome-linux)
-   [Awesome-Linux-Software](https://github.com/luongvo209/Awesome-Linux-Software/blob/master/README_zh-CN.md)
-   [awesome-linux-containers](https://github.com/Friz-zy/awesome-linux-containers)

-   [awesome-shell](https://github.com/alebcay/awesome-shell)
-   [awesome-fish-shell](https://github.com/jorgebucaran/awesome-fish-shell)

### C & CPP

-   [C 语言资源大全中文版](https://github.com/jobbole/awesome-c-cn)
-   [C++ 资源大全中文版](https://github.com/jobbole/awesome-cpp-cn)
-   [awesome-c](https://github.com/kozross/awesome-c)
-   [awesome-cpp](https://github.com/fffaraz/awesome-cpp)
-   [awesome-modern-cpp](https://github.com/rigtorp/awesome-modern-cpp)

### IOS

-   [iOS 资源大全中文版](https://github.com/jobbole/awesome-ios-cn)
-   [awesome-ios](https://github.com/vsouza/awesome-ios)
-   [awesome-ios-ui](https://github.com/cjwirth/awesome-ios-ui)
-   [Swift 开源精选资源](https://github.com/ipader/awesome-swift)
-   [awesome-swift](https://github.com/matteocrippa/awesome-swift)

### Python

-   [推荐 Python 技术书](https://github.com/jobbole/awesome-python-books)
-   [Python 资源大全中文版](https://github.com/jobbole/awesome-python-cn)
-   [Django 优秀资源大全](https://github.com/haiiiiiyun/awesome-django-cn)
-   [PyQt/PySide 中文开发者必备资料合集](https://github.com/chroming/awesome-pyqt-cn)
-   [awesome-python](https://github.com/vinta/awesome-python)
-   [awesome-python-applications](https://github.com/mahmoud/awesome-python-applications)
-   [awesome-python-books](https://github.com/Junnplus/awesome-python-books)

### Java

-   [Java 资源大全中文版](https://github.com/jobbole/awesome-java-cn)
-   [Java 面试题合集](https://github.com/jobbole/java-interview)
-   [awesome-java](https://github.com/akullpp/awesome-java)
-   [awesome-maven](https://github.com/CloudsDocker/awesome-maven)
-   [awesome-gradle](https://github.com/ksoichiro/awesome-gradle)
-   [awesome-gradle-plugins](https://github.com/aalmiray/awesome-gradle-plugins)

-   [awesome-spring](https://github.com/bsdfzzzy/awesome-spring)
-   [awesome-spring-boot](https://github.com/ityouknow/awesome-spring-boot)
-   [stunstunstun/awesome-spring-boot](https://github.com/stunstunstun/awesome-spring-boot)
-   [CFMystery/awesome-spring-boot](https://github.com/CFMystery/awesome-spring-boot)
-   [awesome-spring-cloud](https://github.com/ityouknow/awesome-spring-cloud)

### Android

-   [Android 资源大全中文版](https://github.com/jobbole/awesome-android-cn)
-   [awesome-android](https://github.com/JStumpp/awesome-android)
-   [awesome-android-ui](https://github.com/wasabeef/awesome-android-ui)
-   [Kotlin 资源大全](https://github.com/xitu/awesome-kotlin-cn)
-   [awesome-kotlin](https://github.com/KotlinBy/awesome-kotlin)
-   [awesome-kotlin-android](https://github.com/adisonhuang/awesome-kotlin-android)

### Microservices

-   [Docker 优秀资源集锦](https://github.com/haiiiiiyun/awesome-docker-cn)
-   [awesome-microservices](https://github.com/mfornos/awesome-microservices)

### Front-End

-   [推荐前端技术书](https://github.com/jobbole/awesome-web-dev-books)
-   [前端技能栈](https://github.com/jobbole/web-skill-set)
-   [前端开发指南](https://github.com/icepy/Front-End-Develop-Guide)
-   [前端开发面试题](https://github.com/markyun/My-blog/tree/master/Front-end-Developer-Questions/Question)
-   [前端技术架构图谱](https://github.com/f2e-awesome/knowledge)
-   [awesome-html5](https://github.com/diegocard/awesome-html5)
-   [CSS 资源大全中文版](https://github.com/jobbole/awesome-css-cn)
-   [awesome-css](https://github.com/awesome-css-group/awesome-css)
-   [JavaScript 资源大全中文版](https://github.com/jobbole/awesome-javascript-cn)
-   [awesome-javascript](https://github.com/sorrycc/awesome-javascript)
-   [awesome-es6](https://github.com/naf3a/awesome-es6)
-   [Nodejs 资源大全中文版](https://github.com/Pines-Cheng/awesome-nodejs-cn)
-   [awesome-nodejs](https://github.com/sindresorhus/awesome-nodejs)
-   [webpack 优秀中文文章](https://github.com/webpack-china/awesome-webpack-cn)
-   [awesome-webpack](https://github.com/webpack-contrib/awesome-webpack)
-   [Gulp 资料大全](https://github.com/Pines-Cheng/awesome-gulp-cn)
-   [awesome-typescript](https://github.com/dzharii/awesome-typescript)
-   [awesome-less](https://github.com/LucasBassetti/awesome-less)

### React

-   [react 中文资源大全](https://github.com/Pines-Cheng/awesome-react-cn)
-   [awesome-react](https://github.com/enaqx/awesome-react)
-   [awesome-react-native](https://github.com/jondot/awesome-react-native)
-   [awesome-react-components](https://github.com/brillout/awesome-react-components)

### Vue

-   [Vue.js 资源大全](https://github.com/Pines-Cheng/awesome-vue-cn)
-   [awesome-vue](https://github.com/vuejs/awesome-vue)
-   [awesome-github-vue](https://github.com/opendigg/awesome-github-vue)

### Angular

-   [中文的 Awesome Angular2 资源列表](https://github.com/zuoez02/awesome-angular2-cn)
-   [awesome-angular](https://github.com/gdi2290/awesome-angular)
-   [awesome-angularjs](https://github.com/gianarb/awesome-angularjs)
-   [awesome-angular-components](https://github.com/brillout/awesome-angular-components)

### Machine Learning

-   [机器学习资源大全中文版](https://github.com/jobbole/awesome-machine-learning-cn)
-   [awesome-machine-learning](https://github.com/josephmisiti/awesome-machine-learning)
-   [awesome-machine-learning-on-source-code](https://github.com/src-d/awesome-machine-learning-on-source-code)
-   [awesome-deep-learning-papers](https://github.com/terryum/awesome-deep-learning-papers)
-   [awesome-deep-learning](https://github.com/ChristosChristofidis/awesome-deep-learning)
-   [awesome-deeplearning-resources](https://github.com/endymecy/awesome-deeplearning-resources)
-   [中文自然语言处理相关资料](https://github.com/crownpku/Awesome-Chinese-NLP)
-   [papers-we-love](https://github.com/papers-we-love/papers-we-love)

### Big Data

-   [awesome-bigdata](https://github.com/onurakpolat/awesome-bigdata)
-   [Elasticsearch 中文相关资料](https://github.com/ginobefun/awesome-elasticsearch-cn)
-   [spark 学习资源收集](https://github.com/backstudy/awesome-spark-cn)

### IoT

-   [phodal/awesome-iot](https://github.com/phodal/awesome-iot)
-   [HQarroum/awesome-iot](https://github.com/HQarroum/awesome-iot)

### Blockchains

-   [区块链(BlockChain)技术开发相关资料](https://github.com/chaozh/awesome-blockchain-cn)
-   [收集所有区块链(BlockChain)相关资料](https://github.com/suixinio/awesome-blockchain-cn)
-   [awesome-blockchains](https://github.com/openblockchains/awesome-blockchains)

### Database

-   [MySQL 资源大全中文版](https://github.com/jobbole/awesome-mysql-cn)
-   [awesome-mysql](https://github.com/shlomi-noach/awesome-mysql)
-   [awesome-db](https://github.com/numetriclabz/awesome-db)
-   [awesome-time-series-database](https://github.com/xephonhq/awesome-time-series-database)

### Go

-   [Go 资源大全中文版](https://github.com/jobbole/awesome-go-cn)
-   [Awesome Go 项目的中文翻译](https://github.com/keen20/awesome-go-cn)
-   [go 语言的中文学习资料](https://github.com/wolfhong/awesome-golang-cn)

### Others

-   [awesome-cheatsheets](https://github.com/LeCoupa/awesome-cheatsheets)
-   [awesome-cheatsheet](https://github.com/detailyang/awesome-cheatsheet)
-   [成为专业程序员路上用到的各种优秀资料、神器及框架](https://github.com/stanzhai/be-a-professional-programmer)
-   [最好用的中文速查表](https://github.com/skywind3000/awesome-cheatsheets)
-   [设计师资源大全](https://github.com/jobbole/awesome-design-cn)
-   [系统管理员资源大全中文版](https://github.com/jobbole/awesome-sysadmin-cn)
-   [游戏服务器资源大全](https://github.com/hstcscolor/awesome-gameserver-cn)
-   [开源网络爬虫资源大全](https://github.com/liinnux/awesome-crawler-cn)
-   [Erlang 资源大全中文版](https://github.com/hstcscolor/awesome-erlang-cn)
-   [Awesome Sublime Cn](https://github.com/sublime-china/awesome-sublime-cn)
-   [超赞的 Linux 软件](https://github.com/zpy807/Awesome-Linux-Software-zh_CN)
-   [Rust 资源大全](https://github.com/zzy/awesome-rust-cn)
-   [DotNet 资源大全中文版](https://github.com/jobbole/awesome-dotnet-cn)
-   [PHP 资源大全中文版](https://github.com/jobbole/awesome-php-cn)
