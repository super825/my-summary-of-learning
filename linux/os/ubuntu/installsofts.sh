#sudo apt update
#sudo apt upgrade -y
#自定义别名
echo "alias cls='clear'" >> ~/.bashrc
echo "alias apt='sudo apt'" >> ~/.bashrc
echo "alias apti='apt install -y'" >> ~/.bashrc
echo "alias npm='sudo npm'" >> ~/.bashrc
echo "alias cnpm='sudo cnpm'" >> ~/.bashrc
echo "alias yarn='sudo yarn'" >> ~/.bashrc
echo "alias pip='sudo pip'" >> ~/.bashrc
echo "alias l='ls -al'" >> ~/.bashrc
source ~/.bashrc
#sudo apt install -y unity-tweak-tool
#输入法
# sudo apt install -y fcitx fcitx-table-wubi
# sudo apt install -y ibus ibus-table-wubi
#常用开发工具
sudo apt install -y chromium-browser
# sudo apt install -y adobe-flashplugin
# sudo apt install -y browser-plugin-freshplayer-libpdf browser-plugin-freshplayer-nacl browser-plugin-freshplayer-pepperflash browser-plugin-gnash
sudo apt install -y wget curl git vim tree
sudo apt install -y cpp gcc g++ gdb
sudo apt install -y default-jdk
sudo apt install -y glibc-doc manpages manpages-dev manpages-posix manpages-posix-dev manpages-zh
#nodejs相关
sudo apt install -y nodejs npm
sudo npm config set registry https://registry.npm.taobao.org
sudo npm i -g npm
sudo npm i -g cnpm --registry=https://registry.npm.taobao.org
sudo npm i -g yarn
yarn config set registry https://registry.npm.taobao.org
#python相关
sudo apt install -y python python-pip python-setuptools ipython python3 python3-pip python3-setuptools ipython3
sudo mkdir ~/.pip
sudo touch ~/.pip/pip.conf
sudo echo -e "[global]\nindex-url = https://pypi.tuna.tsinghua.edu.cn/simple" > ~/.pip/pip.conf
sudo pip install --upgrade pip
sudo pip install tldr
sudo tldr --update
#sudo pip install pipenv
#sudo pip install numpy scipy matplotlib pandas scikit-learn
sudo pip3 install --upgrade pip
sudo pip3 install tldr
#sudo pip3 install pipenv
#sudo pip3 install numpy scipy matplotlib pandas scikit-learn
#语法检查
pip install --user flake8
#自动代码提示
pip install --user jedi
#自动导入模块
pip install --user isort
#代码格式化
pip install --user yapf
#通过安装 pygments扩展让vim支持 50+ 种语言（包括 go/rust/scala 等，基本覆盖所有主流语言
sudo pip install pygments
#vscode
sudo apt install -y code
#fish & oh my fish
sudo apt install -y fish
curl -L https://get.oh-my.fish | fish
#zsh & oh my zsh
#sudo apt install -y zsh
#sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
#自定义别名
#echo "alias cls='clear'" >> ~/.zshrc
#echo "alias apt='sudo apt'" >> ~/.zshrc
#echo "alias apti='apt install -y'" >> ~/.zshrc
#echo "alias npm='sudo npm'" >> ~/.zshrc
#echo "alias cnpm='sudo cnpm'" >> ~/.zshrc
#echo "alias yarn='sudo yarn'" >> ~/.zshrc
#echo "alias pip='sudo pip'" >> ~/.zshrc
#echo "alias l='ls -al'" >> ~/.zshrc
#source ~/.zshrc
#超好用的vim配置
#wget -qO- https://raw.github.com/ma6174/vim/master/setup.sh | sh -x
#space-vim
#sudo curl -sLf https://spacevim.org/cn/install.sh | bash
#手动安装deb包
#sudo apt-get install -f
#sudo dpkg -i deb文件名
#升级所有过期库
#pip list --outdated | awk '{print "pip install --upgrade "$1}' > python-upgrade.sh
#sh python-upgrade.sh
#重新安装pip
#https://www.imooc.com/article/31953?block_id=tuijian_wz
#sudo python3 -m pip uninstall pip && sudo apt install python3-pip --reinstall