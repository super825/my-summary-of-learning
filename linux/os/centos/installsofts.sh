#自定义别名
echo "alias cls='clear'" >> ~/.bashrc
echo "alias yum='sudo yum'" >> ~/.bashrc
echo "alias yumi='yum install -y'" >> ~/.bashrc
echo "alias npm='sudo npm'" >> ~/.bashrc
echo "alias cnpm='sudo cnpm'" >> ~/.bashrc
echo "alias yarn='sudo yarn'" >> ~/.bashrc
echo "alias pip='sudo pip'" >> ~/.bashrc
echo "alias l='ls -al'" >> ~/.bashrc
source ~/.bashrc
#mirror
#https://linux.cn/article-9680-1.html
#在 CentOS 6 系统上安装最新版 Python3 软件包的 3 种方法
sudo yum install -y yum-fastestmirror
# sudo yum install -y yum-axelget
# sudo yum install -y yumex curl
#使用 EPEL 源 (Extra Packages for Enterprise Linux）
sudo yum install -y epel-release
#使用 Software Collections 源 （SCL）
# sudo yum install -y centos-release-scl
#yum info rh-python35
#yum install rh-python35
#scl enable rh-python35 bash
#scl -l
#使用 IUS 社区源
# curl 'https://setup.ius.io/' -o setup-ius.sh
# sudo sh setup-ius.sh
#wget http://rpms.remirepo.net/enterprise/remi-release-7.rpm
#sudo rpm -Uvh remi-release-7.rpm
#sudo yum --enablerepo=remi update remi-release
#输入法
sudo yum install -y ibus ibus-table-wubi 
#sudo yum install -y fcitx-configtool fcitx fcitx-table-wubi
#打开输入法设置
# gsettings set org.gnome.settings-daemon.plugins.keyboard active false
#常用开发工具
sudo yum install -y chromium
sudo yum install -y vim vim-x11 neovim
sudo yum install -y wget curl git
sudo yum install -y kernel-devel
sudo yum install -y gcc
sudo yum install -y cpp
sudo yum install -y gcc-c++
sudo yum install -y gd-devel
sudo yum install -y ncurses
sudo yum install -y ncurses-devel
sudo yum install -y zlib zlib-devel
sudo yum install -y freetype-devel freetype-demos freetype-utils
sudo yum install -y openssl openssl-devel
sudo yum install -y bzip2-devel
sudo yum install -y sqlite-devel
sudo yum install -y python-devel
sudo yum install -y java-1.8.0-openjdk.x86_64 java-1.8.0-openjdk-devel.x86_64
sudo yum install -y glibc-doc manpages-posix-dev
sudo yum install -y man-pages man2html help2man
#python相关
sudo yum install -y python python-ipython python-pip python-setuptools
sudo pip install --upgrade pip
#yum --enablerepo=ius install -y python36u python36u-pip
#升级所有过期库
# sudo pip list --outdated | awk '{print "sudo pip install --upgrade "$1}' > python-upgrade.sh
# sudo sh python-upgrade.sh
#sudo yum install -y python2-numpy python2-pandas python2-matplotlib scipy python2-jupyter-core
sudo mkdir ~/.pip
sudo touch ~/.pip/pip.conf
sudo echo -e "[global]\nindex-url = https://pypi.tuna.tsinghua.edu.cn/simple" > ~/.pip/pip.conf
#wget https://bootstrap.pypa.io/get-pip.py
#sudo python get-pip.py
#sudo pip install cheat tldr
#sudo pip install ipython
#nodejs相关
sudo yum install -y nodejs npm
sudo npm config set registry https://registry.npm.taobao.org
sudo npm i -g tldr
sudo tldr --update
sudo npm i -g cnpm --registry https://registry.npm.taobao.org
#sudo wget https://dl.yarnpkg.com/rpm/yarn.repo -O /etc/yum.repos.d/yarn.repo
sudo npm i -g yarn
sudo yarn config set registry https://registry.npm.taobao.org
#vscode
sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
sudo sh -c 'echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/vscode.repo'
sudo yum check-update
sudo yum install -y code
#fish & oh my fish
sudo yum install -y fish
curl -L https://get.oh-my.fish | fish
#zsh相关
#sudo yum install -y zsh
#sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
#自定义别名
#echo "alias cls='clear'" >> ~/.zshrc
#echo "alias yum='sudo yum'" >> ~/.zshrc
#echo "alias yumi='yum install -y'" >> ~/.zshrc
#echo "alias npm='sudo npm'" >> ~/.zshrc
#echo "alias cnpm='sudo cnpm'" >> ~/.zshrc
#echo "alias yarn='sudo yarn'" >> ~/.zshrc
#echo "alias pip='sudo pip'" >> ~/.zshrc
#echo "alias l='ls -al'" >> ~/.zshrc
#source ~/.zshrc
#vim config
# wget -qO- https://raw.github.com/ma6174/vim/master/setup.sh | sh -x
#space-vim
#sudo curl -sLf https://spacevim.org/cn/install.sh | bash
#yum grouplist | more